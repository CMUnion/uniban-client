package cat.melon.MelonBan.Packets;

import cat.melon.MelonBan.Utils.AESEnctyper;

public abstract class PacketIdentify {
    private PacketType type;
    private int uid;
    private String token;
    
    public PacketIdentify(PacketType packetType, int clientUid, AESEnctyper enctyper) {
        type = packetType;
        uid = clientUid;
        token = enctyper.encrypt(enctyper.getRandomString(20).concat("Hello"));
    }

    public PacketIdentify() {}

    public final PacketType getType() {
        return type;
    }

    public final int getUID() {
        return uid;
    }

    public final String getToken() {
        return token;
    }
}
