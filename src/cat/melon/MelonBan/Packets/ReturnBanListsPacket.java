package cat.melon.MelonBan.Packets;

import cat.melon.MelonBan.PluginData.CloudList;
import cat.melon.MelonBan.Utils.AESEnctyper;

public class ReturnBanListsPacket extends PacketIdentify implements Packet{
    CloudList cloudList;

    public ReturnBanListsPacket(int UID, AESEnctyper enctyper, CloudList list){
        super(PacketType.Ban_List_Packet, UID, enctyper);
        this.cloudList = list;
    }

    public ReturnBanListsPacket() {}

    public CloudList getCloudList() {
        return cloudList;
    }

}
