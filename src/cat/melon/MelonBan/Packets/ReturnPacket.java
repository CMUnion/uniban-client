package cat.melon.MelonBan.Packets;

import cat.melon.MelonBan.Utils.AESEnctyper;

public class ReturnPacket extends PacketIdentify implements Packet {
    public int status;

    public ReturnPacket(int UID, AESEnctyper enctyper, int status){
        super(PacketType.RETURN, UID, enctyper);
        this.status = status;
    }

    public ReturnPacket() {}

    public int getStatus(){
        return status;
    }
}
