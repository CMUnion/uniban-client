package cat.melon.MelonBan.Packets;

public enum PacketType {
    FETCH_LIST(0),PUSH_LIST(1),Ban_List_Packet(2),RETURN(3),SERVER_NAME_PACKET(4);

    private int type;

    PacketType(int type){
        this.type=type;
    }

    public int getType() {
        return type;
    }

    @Override
    public String toString() {
        return new StringBuilder().append(type).toString();
    }
}
