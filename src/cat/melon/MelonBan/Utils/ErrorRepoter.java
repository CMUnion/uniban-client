package cat.melon.MelonBan.Utils;

import org.bukkit.plugin.Plugin;

import cat.melon.MelonBan.Main;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

public class ErrorRepoter {
    private Logger logger = null;
    StringBuilder sb = new StringBuilder();
    Map<String, String> spigotinfo = new HashMap<>();
    Main plugin = null;
    private String notice = "\n" + plugin.getLang("reporter.tellus") + "\n" +
            plugin.getLang("reporter.notice") + "\n" +
            plugin.getLang("reporter.howto") + "\n";

    private String begin = "\n************************** BEGIN ERROR TRACKER ***************************\n";

    private String end = "\n*************************** END ERROR TRACKER ****************************";

    public ErrorRepoter(Logger logger, Main plugin) {
        this.logger = logger;
        spigotinfo.put("name", plugin.getServer().getName());
        spigotinfo.put("bukkitversion", plugin.getServer().getBukkitVersion());
        spigotinfo.put("version", plugin.getServer().getVersion());
        for (Plugin x : plugin.getServer().getPluginManager().getPlugins()) {
            sb.append(x.getName());
            sb.append(", ");
        }
        sb.delete(sb.length() - 2, sb.length());
        spigotinfo.put("plugins", sb.toString());
        sb.delete(0, sb.length());
        this.plugin = plugin;
    }

    public <E extends Exception> void printStackTrace(E exception) {
        sb.append(notice).append(begin);
        Properties props = System.getProperties();
        sb.append("OS: " + props.getProperty("os.name") + " " + props.getProperty("os.version") + " " + props.getProperty("os.arch") + "\n");
        sb.append("Java: " + props.getProperty("java.version") + " \n");
        sb.append("Server: " + spigotinfo.get("name") + " " + spigotinfo.get("version") + "\n");
        sb.append("Bukkit Version: " + spigotinfo.get("bukkitversion") + "\n");
        sb.append("Plugins: " + spigotinfo.get("plugins") + "\n\n");
        for (StackTraceElement x : exception.getStackTrace()) {
            sb.append(x);
        }
        sb.append(end);

        logger.warning(sb.toString());
        sb.delete(0, sb.length());
    }
}
