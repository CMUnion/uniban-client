package cat.melon.MelonBan;

import cat.melon.MelonBan.PluginData.CloudList;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import cat.melon.MelonBan.Utils.ErrorRepoter;
import cat.melon.MelonBan.Utils.LocaleAPI;
import cat.melon.MelonBan.Utils.LocaleAPI.Opcodes;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Main extends JavaPlugin {
    private static final String DEFAULT_LOCALE = "en_US";

    protected FileConfiguration config = null;
    private LocaleAPI.LocaleData lang; // private -> getter()
    private Map<UUID,Integer> bannedby = new HashMap<>();
    private Map<Integer,String> servernames = new HashMap<>();
    private Client client = null;
    public ErrorRepoter er = new ErrorRepoter(this.getLogger(), this);
    private ArrayList<CloudList> cloudlist = new ArrayList<>();
    File dataFolder = null;
    //Folder not floder
    private LocaleAPI.LocaleData defaultLang;

    public String getLang(String languagePath) {
        return LocaleAPI.localizeAt(languagePath, lang, defaultLang);
    }

    @Override
    public void onEnable() {
        dataFolder = this.getDataFolder();
        config = this.getConfig();

        // Setup default language
        this.defaultLang = LocaleAPI.localizeDataFrom(this, DEFAULT_LOCALE);

        // Apply desired language
        String desiredLanguage = config.getString("config.language");
        this.lang = LocaleAPI.localizeDataFrom(this, desiredLanguage, file -> {
            if (file.exists())
                return Opcodes.CONTINUE;

            Bukkit.getLogger().warning("Language " + desiredLanguage + " is not supported, use default " + DEFAULT_LOCALE);
            return LocaleAPI.localizeDataFrom(this, DEFAULT_LOCALE);
        });

        client = new Client(config.getString("server.address"), config.getInt("server.port"), config.getInt("account.id"), config.getString("account.token"),this);


        new BukkitRunnable() {
            @Override
            public void run() {
                cloudlist = client.fetchBanLists(config.getIntegerList("config.usinglists"));
            }
        }.runTaskTimer(this, config.getInt("config.sync.fetch") * 20L, config.getInt("config.sync.fetch") * 20L);


        //TODO
        /*
         * The day is light and the night is dark.
         * And there was a cat running with a hat.
         * On the hat there was a lamp.
         * The little lamp made the lonely night become bright.
         * After the hat cat, the boy is ridding the bike.
         * Because of the dog is running, pursue the cat's hat.
         * The cat is lovely, the boy is gallant.
         * On the backseat, a ball is sleeping in the midnight.
         * The woman was printed on the ball and she is tall.
         * Tall woman is kind and she has three hands.
         * Focus on her one hand and there is a blue glass.
         * Inside the blue glass there is some orange juice.
         * The glass is clear and the juice is sweet.
         * The best choice is drinking it with some pancake.
         * Pancake is crispy, and round like the ball that printed the pic.
         * The boy threw the ball and the ball hit the grounds beside the dog.
         * Dog caught it and ran off with a woof.
         * The boy and the cat was safety and then they went home.
         *
         * */

    }

    public Map<UUID, Integer> getBannedbyMap() {
        return bannedby;
    }

    public Map<Integer, String> getServernamesMap() {
        return servernames;
    }

    public void setServernamesMap(Map<Integer, String> names){
        servernames=names;
    }

    public void setBannedbyMap(Map<UUID, Integer> bannedbyList){
        bannedby = bannedbyList;
    }
}
