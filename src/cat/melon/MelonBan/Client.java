package cat.melon.MelonBan;

import cat.melon.MelonBan.Packets.*;
import cat.melon.MelonBan.PluginData.BannedPlayer;
import cat.melon.MelonBan.PluginData.CloudList;
import cat.melon.MelonBan.Utils.AESEnctyper;
import com.google.gson.Gson;
import com.mysql.fabric.xmlrpc.base.Array;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class Client {
    private Main plugin;
    private Gson gson = new Gson();
    private int ClientID;
    private String ClientToken;
    private String address;
    private int port;
    private StringBuilder sb = new StringBuilder();
    private AESEnctyper enctyper;

    /**
     * The same as <code>new Socket(address, port);</code>
     *
     * @return A socket using this.address and this.port.
     */
    private Socket createNewSocket() throws IOException {
        return new Socket(address, port);
    }

    public Client(String address, int port, int ID, String token,Main plugin) {
        this.ClientID = ID;
        this.ClientToken = token;
        this.enctyper = new AESEnctyper(ClientToken);
        this.address = address;
        this.port = port;
    }

    /**
     * @return Message from server. This method will return null if any exception occur.
     * //from not form. don't make mistakes(
     */
    public <APacket extends Packet> ArrayList<Packet> sendPacket(APacket packet) {

        try {
            Socket connect = this.createNewSocket();
            OutputStream os = connect.getOutputStream();
            InputStream is = connect.getInputStream();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
            String finalMessage = gson.toJson(packet);
            bw.write(finalMessage);
            bw.flush();
            connect.shutdownOutput();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String re;
            sb.delete(0, sb.length());
            while (!((re = br.readLine()) == null)) {
                sb.append(re).append('\n');
            }
            re = sb.toString(); //multi-packet support
            String[] res = re.split("\n");
            ArrayList<Packet> packets = new ArrayList<>();
            for (String x : res) {
                packets.add(gson.fromJson(x, Packet.class));
            }
            return packets;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }


    public void resetConfig(String address, int port, int ID, String token) {
        this.ClientID = ID;
        this.ClientToken = token;
        this.enctyper = new AESEnctyper(ClientToken);
        this.address = address;
        this.port = port;
    }


    public boolean fetchBanLists(List<Integer> baseIdList) {
        ArrayList<Packet> re = this.sendPacket(new FetchPacket(ClientID, enctyper, baseIdList));
        if (re.get(0) instanceof ReturnPacket && re.get(1) instanceof ServerNamePacket && re.get(2) instanceof ReturnBanListsPacket) {
            ArrayList<BannedPlayer> bp = new ArrayList<>();

            plugin.setServernamesMap(((ServerNamePacket)re.get(1)).getServernames());
            re.remove(0);

            Map<UUID,Integer> tmp= new HashMap<>();
            for (Packet x : re) {

                for(BannedPlayer p:((ReturnBanListsPacket) x).getCloudList().getPlayers()){
                    tmp.put(p.uuid,((ReturnBanListsPacket) x).getCloudList().getUID());
                    if(bp.indexOf(p) != -1) bp.add(p);
                }
            }
            plugin.setBannedbyMap(tmp);
            return true;
        }
        return false;
    }

    public int pushBanLists(ArrayList<BannedPlayer> banlist) {
        ArrayList<Packet> re = this.sendPacket(new PushBanlistPacket(ClientID,enctyper,banlist));
        return ((ReturnPacket)re.get(0)).getStatus();
    }
}
